<!-- php -S localhost:8000 -->
<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Activity</title>
</head>

<body>
    <h1>Divisible of Five</h1>
    <p><?php printDivisibleOfFive() ?></p>
    <p><?php array_push($students, 'Zed') ?></p>
    <p><?php var_dump($students) ?></p>
    <p><?php echo count($students) ?></p>

    <p><?php array_push($students, 'Ian') ?></p>
    <p><?php var_dump($students) ?></p>
    <p><?php echo count($students) ?></p>

    <p><?php array_shift($students) ?></p>
    <p><?php var_dump($students) ?></p>
    <p><?php echo count($students) ?></p>
</body>

</html>